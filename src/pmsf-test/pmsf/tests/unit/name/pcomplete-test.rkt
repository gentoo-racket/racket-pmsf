;; This file is part of racket-pmsf - PMS formatting and parsing functions.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-pmsf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-pmsf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-pmsf.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base


(module+ test
  (require rackunit
           pmsf/name)

  (test-equal? "basic string to pcomplete"
               (string->pcomplete "app-misc/asd-1")
               (pcomplete "app-misc"
                          (pfull "asd" (pversion "1" '() '() '() '() '() 0))))

  (let ([str
         "app-misc/asd-1.2.3_alpha_p20220101_p1-r1"])
    (test-equal? "pcomplete <-> string"
                 (pcomplete->string (string->pcomplete str))
                 str))

  (test-exn "fail to parse \"//\" (double slash)"
            exn:fail?
            (lambda () (string->pcomplete "app-misc//asd-1")))

  ;; Some selected complete package names from the ::gentoo tree.
  (let ([pkgs
         (list "acct-group/_cron-failure-0"
               "app-arch/advancecomp-2.3"
               "app-backup/amanda-3.5.1-r3"
               "app-i18n/anthy-9100h-r3"
               "app-misc/FreeSSM-1.2.5_p20210702"
               "app-vim/Vim-Jinja2-Syntax-0.0_pre20210604"
               "dev-tcltk/blt-2.5.3-r1"
               "dev-tex/abntex-0.9_beta2-r1"
               "dev-texlive/texlive-basic-2021"
               "games-fps/alephone-20220115-r1"
               "games-misc/asr-manpages-1.3_rc6"
               "kde-apps/akonadi-22.04.3"
               "net-dialup/accel-ppp-1.12.0_p20210430"
               "net-libs/NativeThread-0_pre20190914-r1"
               "sci-libs/NNPACK-2020.12.22")])
    (for ([p pkgs])
      (test-equal? (format "Parse complete package name ~v" p)
                   (pcomplete->string (string->pcomplete p))
                   p))))
