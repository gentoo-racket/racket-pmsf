;; This file is part of racket-pmsf - PMS formatting and parsing functions.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-pmsf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-pmsf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-pmsf.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base


(module+ test
  (require rackunit
           pmsf/iuse)

  (test-equal? "to piuse: -X"
               (string->piuse "-X")
               (piuse 'disabled "X"))

  (test-equal? "to piuse: +wayland"
               (string->piuse "+wayland")
               (piuse 'enabled "wayland"))

  (test-equal? "to piuse: jit"
               (string->piuse "jit")
               (piuse #false "jit"))

  (test-equal? "piuse <-> string: -X"
               (piuse->string (string->piuse "-X"))
               "-X")

  (test-equal? "piuse <-> string: +wayland"
               (piuse->string (string->piuse "+wayland"))
               "+wayland")

  (test-equal? "piuse <-> string: jit"
               (piuse->string (string->piuse "jit"))
               "jit"))
