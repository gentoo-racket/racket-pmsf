;; This file is part of racket-pmsf - PMS formatting and parsing functions.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-pmsf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-pmsf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-pmsf.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base

(require "struct.rkt")

(provide (all-defined-out))


(define (pcondition->contents input-pcondition)
  (let loop
      ([input-contents
        (pcondition-contents input-pcondition)]
       [output-contents
        '()])
    (cond
      [(null? input-contents)
       output-contents]
      [else
       (loop (cdr input-contents)
             (append output-contents
                     (cond
                       [(pcondition? (car input-contents))
                        (pcondition->contents (car input-contents))]
                       [else
                        (list (car input-contents))])))])))
