#lang info


(define pkg-desc "PMS formatting and parsing functions. Condition component.")

(define version "3.2")

(define pkg-authors '(xgqt))

(define license 'GPL-2.0-or-later)

(define collection 'multi)

(define deps
  '("base"
    "reprovide-lang-lib"
    "threading-lib"))

(define build-deps
  '())
