;; This file is part of racket-pmsf - PMS formatting and parsing functions.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-pmsf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-pmsf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-pmsf.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base

(require racket/contract/base
         (only-in racket/list take)
         (only-in racket/string string-join)
         "pname.rkt"
         "pversion.rkt")

(provide (all-defined-out))


(struct pfull
  (name version)
  #:guard (struct-guard/c pname? (or/c #false pversion?))
  #:transparent)


(define revision-regexp
  #rx"-r[0-9]+$")


(define (pfull->string input-pfull)
  (string-append (pfull-name input-pfull)
                 (let ([version
                        (pfull-version input-pfull)])
                   (cond
                     [version
                      (string-append "-"
                                     (pversion->string version))]
                     [else
                      ""]))))

(define (string->pfull input-string)
  (let ([revision
         (regexp-match revision-regexp input-string)]
        [string-no-revision
         (regexp-replace revision-regexp input-string "")])
    (cond
      [(and (not revision)
            (pname? string-no-revision))
       (pfull input-string #false)]
      [else
       (let* ([splitted-reversed
               (reverse (regexp-split #rx"-" string-no-revision))]
              [version-string
               (string-append (car splitted-reversed)
                              (cond
                                [revision
                                 =>
                                 car]
                                [else
                                 ""]))]
              [name-string
               (regexp-replace (regexp (string-append "-" version-string "$"))
                               input-string
                               "")])
         (pfull name-string
                (string->pversion version-string)))])))
