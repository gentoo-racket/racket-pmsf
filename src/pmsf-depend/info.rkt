#lang info


(define pkg-desc "PMS formatting and parsing functions. DEPEND component.")

(define version "3.2")

(define pkg-authors '(xgqt))

(define license 'GPL-2.0-or-later)

(define collection 'multi)

(define deps
  '("base"
    "brag-lib"
    "reprovide-lang-lib"
    "threading-lib"
    "pmsf-condition"
    "pmsf-lib"
    "pmsf-name"
    "pmsf-slot"))

(define build-deps
  '())
