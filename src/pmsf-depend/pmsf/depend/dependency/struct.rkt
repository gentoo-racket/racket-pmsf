;; This file is part of racket-pmsf - PMS formatting and parsing functions.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-pmsf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-pmsf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-pmsf.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base

(require racket/contract/base
         pmsf/name
         pmsf/slot)

(provide (all-defined-out))


#|
>=         app-misc/asd :     0    =         [guile,python]
constraint complete     (slot name operator) (flag     ...)

USE flags ("2-style and 4-style USE dependencies"):
[-guile,!python=,ruby(+)?]
get what is inside "[" and "]", separated with ",",
then parse individual flags

pdependency-block       :  #f  !  !!
pdependency-constraint  :  #f  <  <=  =  >  >=  ~
pslot-operator          :  #f  *  =
pflag-operator          :  #f  =  !=  ?  !?  -  +?  -?
|#


(struct pdepflag
  (name operator default)
  #:guard (struct-guard/c string? (or/c #false symbol?) (or/c #false symbol?))
  #:transparent)

(struct pdependency
  (block constraint complete glob slot slotoperator flags)
  #:guard (struct-guard/c (or/c #false string?)
                          (or/c #false string?)
                          pcomplete?
                          boolean?
                          (or/c #false pslot?)
                          (or/c #false string?)
                          (listof pdepflag?))
  #:transparent)
