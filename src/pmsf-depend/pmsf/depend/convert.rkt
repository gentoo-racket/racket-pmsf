;; This file is part of racket-pmsf - PMS formatting and parsing functions.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-pmsf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-pmsf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-pmsf.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base

(require (only-in racket/string string-join)
         (only-in threading ~>>)
         pmsf/condition
         pmsf/condition/condition-converter
         pmsf/port-converter
         pmsf/string-converter
         "dependency/convert.rkt"
         "interpret.rkt"
         "parse.rkt"
         "struct.rkt"
         "tokenize.rkt")

(provide (all-defined-out))


(define-pcondition-converter pdepend-condition->string
  dependency->string)

(define (dependency->string v)
  (cond
    [(pcondition? v)
     (pdepend-condition->string v)]
    [(list? v)
     (~>> v
          (map dependency->string)
          string-join  ; TODO: Format?
          (format "( ~a )"))]
    [else
     (pdependency->string v)]))

(define-condition-converter pdepend->string
  pdepend-dependencies dependency->string)


(define-port-converter port->pdepend
  tokenize
  parse-to-datum
  interpret)

(define-string-converter string->pdepend
  port->pdepend)
