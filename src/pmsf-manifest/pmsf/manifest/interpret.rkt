;; This file is part of racket-pmsf - PMS formatting and parsing functions.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-pmsf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-pmsf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-pmsf.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base

(require (only-in racket/match match)
         (only-in  srfi/19 string->date)
         "struct.rkt")

(provide interpret)


(define (interpret input-datum-list)
  (for/fold ([timestamp        #false]
             [ignore-list      '()]
             [submanifest-list '()]
             [data-list        '()]
             [dist-list        '()]
             [aux-list         '()]
             #:result (manifest timestamp
                                ignore-list
                                submanifest-list
                                data-list
                                dist-list
                                aux-list))
            ([datum input-datum-list])
    (match datum
      [`(timestamp ,timestamp-string)
       (values (string->date timestamp-string "~Y-~m-~dT~H:~M:~SZ")
               ignore-list
               submanifest-list
               data-list
               dist-list
               aux-list)]
      [`(ignore ,path)
       (values timestamp
               `(,@ignore-list ,path)
               submanifest-list
               data-list
               dist-list
               aux-list)]
      [`(submanifest (pathcheck ,path ,size ,checksums))
       (values timestamp
               ignore-list
               `(,@submanifest-list
                 ,(submanifest path size (lists->checksums checksums)))
               data-list
               dist-list
               aux-list)]
      [`(data (pathcheck ,path ,size ,checksums))
       (values timestamp
               ignore-list
               submanifest-list
               `(,@data-list
                 ,(data path size (lists->checksums checksums)))
               dist-list
               aux-list)]
      [`(dist (pathcheck ,path ,size ,checksums))
       (values timestamp
               ignore-list
               submanifest-list
               data-list
               `(,@dist-list
                 ,(dist path size (lists->checksums checksums)))
               aux-list)]
      [`(aux (pathcheck ,path ,size ,checksums))
       (values timestamp
               ignore-list
               submanifest-list
               data-list
               dist-list
               `(,@aux-list
                 ,(aux path size (lists->checksums checksums))))]
      [record
       (error 'interpret "unmatched record, given ~v" record)])))
