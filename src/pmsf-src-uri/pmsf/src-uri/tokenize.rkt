;; This file is part of racket-pmsf - PMS formatting and parsing functions.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-pmsf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-pmsf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-pmsf.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base

(require brag/support
         (only-in pmsf/condition/condition condition-string?)
         (only-in pmsf/tokenizer define-tokenize))

(provide (all-defined-out))


(define-lex-abbrev lex-string
  (:or alphabetic numeric punctuation symbolic))


(define lex
  (lexer-srcloc
   [#\(
    (token 'OPEN-PAREN)]
   [#\)
    (token 'CLOSE-PAREN)]
   ["->"
    (token 'ARROW)]
   [(:+ lex-string)
    (cond
      [(condition-string? lexeme)
       (token 'CONDITION lexeme)]
      [else
       (token 'SRC-LOCATION lexeme)])]
   [whitespace
    (token 'WHITESPACE lexeme #:skip? #true)]
   [(eof)
    (void)]))


(define-tokenize tokenize
  lex)
