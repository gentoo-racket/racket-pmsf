;; This file is part of racket-pmsf - PMS formatting and parsing functions.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-pmsf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-pmsf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-pmsf.  If not, see <https://www.gnu.org/licenses/>.


#lang typed/racket/base

(require "condition.rkt"
         "name.rkt"
         "slot.rkt")

(require/typed/provide pmsf/depend
  ;; Struct
  [#:struct pdepflag
   ([name                   : String]
    [operator               : (Option Symbol)]
    [default                : (Option Symbol)])
   #:type-name PDepflag]
  [#:struct pdependency
   ([block                  : (Option String)]
    [constraint             : (Option String)]
    [complete               : PComplete]
    [glob                   : Boolean]
    [slot                   : (Option PSlot)]
    [slotoperator           : (Option String)]
    [flags                  : (Listof PDepflag)])
   #:type-name PDependency]
  [#:struct pdepend
   ([dependencies           : (Listof (U (Listof Any) PCondition PDependency))])
   #:type-name PDepend]
  ;; Convert
  [pdependency->string      (-> PDependency String)]
  [string->pdependency      (-> String PDependency)]
  [pdepend->string          (-> PDepend String)]
  [string->pdepend          (-> String PDepend)]
  ;; Query
  [pdepend->dependencies    (-> PDepend PDependency)])

(provide PDepend
         PDependency
         PDepflag)
