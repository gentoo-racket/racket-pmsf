;; This file is part of racket-pmsf - PMS formatting and parsing functions.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-pmsf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-pmsf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-pmsf.  If not, see <https://www.gnu.org/licenses/>.


#lang typed/racket/base

(require (only-in typed/srfi/19 Date))

(require/typed/provide pmsf/manifest
  ;; Structs
  [#:struct checksum
   ([algorithm              : String]
    [sum                    : String])
   #:type-name Checksum]
  [#:struct pathcheck
   ([path                   : Path-String]
    [size                   : Exact-Nonnegative-Integer]
    [checksums              : (Listof Checksum)])
   #:type-name PathCheck]
  [#:struct (submanifest pathcheck)
   ()
   #:type-name SubManifest]
  [#:struct (data pathcheck)
   ()
   #:type-name Data]
  [#:struct (dist pathcheck)
   ()
   #:type-name Dist]
  [#:struct (aux pathcheck)
   ()
   #:type-name Aux]
  [#:struct manifest
   ([timestamp              : Date]
    [ignore                 : (Listof Path-String)]
    [submanifests           : (Listof SubManifest)]
    [data                   : (Listof Data)]
    [dist                   : (Listof Dist)]
    [aux                    : (Listof Aux)])
   #:type-name Manifest]
  ;; Convert
  [checksum->string         (-> Checksum String)]
  [pathcheck->string        (-> PathCheck String String)]
  [timestamp->string        (-> Date String)]
  [submanifest->string      (-> SubManifest String)]
  [ignore->string           (-> Path-String String)]
  [data->string             (-> Data String)]
  [dist->string             (-> Dist String)]
  [aux->string              (-> Aux String)]
  [manifest->string         (-> Manifest String)]
  [string->manifest         (-> String Manifest)])

(provide Checksum
         PathCheck
         SubManifest
         Data
         Dist
         Aux
         Manifest)
