#lang info


(define pkg-desc "PMS formatting and parsing functions. Documentation.")

(define version "3.2")

(define pkg-authors '(xgqt))

(define license 'GPL-2.0-or-later)

(define collection 'multi)

(define deps
  '("base"))

(define build-deps
  '("net-doc"
    "racket-doc"
    "scribble-lib"
    "ziptie-git"
    "pmsf-condition"
    "pmsf-depend"
    "pmsf-iuse"
    "pmsf-keywords"
    "pmsf-lib"
    "pmsf-license"
    "pmsf-manifest"
    "pmsf-name"
    "pmsf-required-use"
    "pmsf-restrict"
    "pmsf-slot"
    "pmsf-src-uri"
    "typed-pmsf"))
