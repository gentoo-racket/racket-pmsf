;; This file is part of racket-pmsf - PMS formatting and parsing functions.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-pmsf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-pmsf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-pmsf.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require (for-label racket
                     pmsf/condition
                     pmsf/restrict)
          scribble/example
          pmsf/restrict)


@(define example-evaluator
   (make-base-eval '(require pmsf/condition pmsf/restrict)))


@title[#:tag "pmsf-restrict"]{RESTRICT}

@defmodule[pmsf/restrict]


@section[#:tag "pmsf-restrict-struct"]{RESTRICT struct}

@defmodule[pmsf/restrict/struct]

@defstruct[
 prestrict
 ([conditions (listof (or/c pcondition? string?))])
 #:transparent
 ]{

 @examples[
 #:eval example-evaluator
 (prestrict (list (pcondition (cons 'not "test") (list "test"))))
 ]
}


@section[#:tag "pmsf-restrict-convert"]{RESTRICT conversion}

@defmodule[pmsf/restrict/convert]

@defproc[
 (prestrict->string [input-prestrict prestrict?])
 string?
 ]{

 @examples[
 #:eval example-evaluator
 (prestrict->string
  (prestrict (list (pcondition (cons 'not "test") (list "test")))))
 ]
}

@defproc[
 (port->prestrict [input-port input-port?])
 prestrict?
 ]{

}

@defproc[
 (string->prestrict [input-string string?])
 prestrict?
 ]{

 @examples[
 #:eval example-evaluator
 (string->prestrict "!test? ( test )")
 ]
}
