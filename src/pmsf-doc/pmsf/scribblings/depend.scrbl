;; This file is part of racket-pmsf - PMS formatting and parsing functions.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-pmsf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-pmsf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-pmsf.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require (for-label racket
                     pmsf/condition
                     pmsf/depend
                     pmsf/name
                     pmsf/slot)
          scribble/example
          pmsf/depend)


@(define example-evaluator
   (make-base-eval '(require pmsf/condition
                             pmsf/depend
                             pmsf/name
                             pmsf/slot)))


@title[#:tag "pmsf-depend"]{DEPEND}

@defmodule[pmsf/depend]


@section[#:tag "pmsf-depend-struct"]{DEPEND structs}

@defstruct[
 pdepflag
 ([name     string?]
  [operator (or/c #false '= '!= '? '!? '-)]
  [default  (or/c #false 'disabled 'enabled)])
 #:transparent
 ]{

 @examples[
 #:eval example-evaluator
 (pdepflag "X" '? 'disabled)
 ]
}

@defstruct[
 pdependency
 ([block        (or/c #false string?)]
  [constraint   (or/c #false string?)]
  [complete     pcomplete?]
  [glob         boolean?]
  [slot         (or/c #false pslot?)]
  [slotoperator (or/c #false string?)]
  [flags        (listof pdepflag?)])
 #:transparent
 ]{

 @examples[
 #:eval example-evaluator
 (pdependency #false
              #false
              (pcomplete "app-misc" (pfull "asd" #false))
              #false
              #false
              #false
              '())
 (pdependency "!!"
              ">="
              (pcomplete "app-misc"
                         (pfull "asd"
                                (pversion "1.2" '() '() '() '() '() 0)))
              #true
              (pslot "0" #false)
              "*"
              (list (pdepflag "X" '? 'disabled)))
 ]
}

@defstruct[
 pdepend
 ([dependencies (listof (or/c list? pcondition? pdependency?))])
 #:transparent
 ]{

 @examples[
 #:eval example-evaluator
 (pdepend
  (list
   (pdependency #false
                #false
                (pcomplete "app-misc" (pfull "asd" #false))
                #false
                #false
                #false
                '())))
 ]
}


@section[#:tag "pmsf-depend-convert"]{DEPEND conversion}

@defmodule[pmsf/depend/convert]

@defproc[
 (pdepend->string
  [input-pdepend pdepend?])
 string?
 ]{

 @examples[
 #:eval example-evaluator
 (pdepend->string
  (pdepend
   (list
    (pdependency #false
                 #false
                 (pcomplete "app-misc" (pfull "asd" #false))
                 #false
                 #false
                 #false
                 '()))))
 ]
}

@defproc[
 (port->pdepend [input-port input-port?])
 pdepend?
 ]{

}

@defproc[
 (string->pdepend [input-string string?])
 pdepend?
 ]{

 @examples[
 #:eval example-evaluator
 (string->pdepend "app-misc/asd")
 (string->pdepend "asd? ( || ( app-misc/asd:= >=app-misc/dsa-1:1=[asd] ) )")
 ]
}


@section[#:tag "pmsf-depend-query"]{DEPEND querying}

@defmodule[pmsf/depend/query]

@defproc[
 (pdepend->dependencies [input-pdepend pdepend?])
 (listof pdependency?)
 ]{
 Extract dependencies from given @racket[pdepend],
 all @racketid[condition]s are thrown away.

 @examples[
 #:eval example-evaluator
 (pdepend->dependencies
  (string->pdepend "asd? ( || ( app-misc/asd:= >=app-misc/dsa-1:1=[asd] ) )"))
 ]
}
