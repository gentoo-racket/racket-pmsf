;; This file is part of racket-pmsf - PMS formatting and parsing functions.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-pmsf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-pmsf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-pmsf.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require (for-label racket
                     pmsf/keywords)
          scribble/example)


@(define example-evaluator
   (make-base-eval '(require pmsf/keywords)))


@title[#:tag "pmsf-keyword"]{KEYWORDS}

@defmodule[pmsf/keywords]


@section[#:tag "pmsf-keyword-struct"]{KEYWORDS structs}

@defmodule[pmsf/keywords/struct]

@defstruct[
 pkeyword
 ([functionality pkeyword-functionality?]
  [architecture  string?]
  [platform      string?])
 #:transparent
 ]{

 @examples[
 #:eval example-evaluator
 (pkeyword 'unstable "amd64" "linux")
 ]
}

@defstruct[
 pkeywords
 ([keywords (listof pkeyword?)])
 #:transparent
 ]{

 @examples[
 #:eval example-evaluator
 (pkeywords (list (pkeyword 'unstable "amd64" "linux")))
 ]
}


@section[#:tag "pmsf-keyword-helpers"]{KEYWORDS helper functions}

@defproc[
 (pkeyword-functionality? [v any])
 boolean?
 ]{

 @examples[
 #:eval example-evaluator
 (pkeyword-functionality? 'stable)
 (pkeyword-functionality? 'unavailable)
 (pkeyword-functionality? 'unstable)
 (pkeyword-functionality? "asd")
 ]
}


@section[#:tag "pmsf-keyword-convert"]{KEYWORDS conversion}

@defmodule[pmsf/keywords/convert]

@defproc[
 (pkeyword-functionality->string [a-pkeyword-functionality pkeyword-functionality?])
 string?
 ]{

 @examples[
 #:eval example-evaluator
 (pkeyword-functionality? 'enabled)
 ]
}

@defproc[
 (pkeyword->string [a-pkeyword pkeyword?])
 string?
 ]{

 @examples[
 #:eval example-evaluator
 (pkeyword->string (pkeyword 'unstable "amd64" "linux"))
 ]
}

@defproc[
 (pkeywords->string [a-pkeywords pkeywords?])
 string?
 ]{

 @examples[
 #:eval example-evaluator
 (pkeywords->string (pkeywords (list (pkeyword 'unstable "amd64" "linux"))))
 ]
}

@defproc[
 (string->pkeyword [a-string string?])
 pkeyword?
 ]{

 @examples[
 #:eval example-evaluator
 (string->pkeyword "~amd64-linux")
 ]
}

@defproc[
 (string->pkeywords [a-string string?])
 pkeywords?
 ]{

 @examples[
 #:eval example-evaluator
 (string->pkeywords "~amd64-linux")
 ]
}


@section[#:tag "pmsf-keyword-query"]{KEYWORDS querying}

@defmodule[pmsf/keywords/query]

@defproc[
 (pkeyword-all? [a-pkeyword pkeyword?])
 boolean?
 ]{

 @examples[
 #:eval example-evaluator
 (pkeyword-all? (pkeyword 'unavailable "*" #false))
 (pkeyword-all? (pkeyword 'unstable "amd64" "linux"))
 ]
}

@defproc[
 (pkeyword-compound? [a-pkeyword pkeyword?])
 boolean?
 ]{

 @examples[
 #:eval example-evaluator
 (pkeyword-compound? (pkeyword 'unstable "amd64" "linux"))
 (pkeyword-compound? (pkeyword 'unstable "amd64" #false))
 ]
}

@defproc[
 (pkeyword-stable? [a-pkeyword pkeyword?])
 boolean?
 ]{

 @examples[
 #:eval example-evaluator
 (pkeyword-stable? (pkeyword 'stable "amd64" "linux"))
 ]
}

@defproc[
 (pkeyword-unavailable? [a-pkeyword pkeyword?])
 boolean?
 ]{

 @examples[
 #:eval example-evaluator
 (pkeyword-unavailable? (pkeyword 'unavailable "amd64" "linux"))
 ]
}

@defproc[
 (pkeyword-unstable? [a-pkeyword pkeyword?])
 boolean?
 ]{

 @examples[
 #:eval example-evaluator
 (pkeyword-unstable? (pkeyword 'unstable "amd64" "linux"))
 ]
}

@defproc[
 (pkeyword-functionality<? [a-pkeyword pkeyword?] [b-pkeyword pkeyword?])
 boolean?
 ]{

 @examples[
 #:eval example-evaluator
 (pkeyword-functionality<? (pkeyword 'unstable "amd64" "linux")
                           (pkeyword 'unavailable "amd64" "linux"))
 ]
}

@defproc[
 (pkeyword-architecture<? [a-pkeyword pkeyword?] [b-pkeyword pkeyword?])
 boolean?
 ]{

 @examples[
 #:eval example-evaluator
 (pkeyword-architecture<? (pkeyword 'unstable "alpha" "linux")
                          (pkeyword 'unstable "amd64" "linux"))
 ]
}

@defproc[
 (pkeyword-platform<? [a-pkeyword pkeyword?] [b-pkeyword pkeyword?])
 boolean?
 ]{

 @examples[
 #:eval example-evaluator
 (pkeyword-platform<? (pkeyword 'unstable "amd64" "bsd")
                      (pkeyword 'unstable "amd64" "linux"))
 ]
}

@defproc[
 (pkeyword<? [a-pkeyword pkeyword?] [b-pkeyword pkeyword?])
 boolean?
 ]{

 @examples[
 #:eval example-evaluator
 (pkeyword<? (pkeyword 'unstable "amd64" #false)
             (pkeyword 'unstable "amd64" "linux"))
 ]
}


@defproc[
 (pkeywords-sort [a-pkeywords pkeywords?])
 pkeywords?
 ]{

 @examples[
 #:eval example-evaluator
 (pkeywords-sort (pkeywords (list (pkeyword 'unstable "x86" #f)
                                  (pkeyword 'unstable "amd64" "linux")
                                  (pkeyword 'unavailable "*" #f)
                                  (pkeyword 'unstable "x64" "macos")
                                  (pkeyword 'unstable "alpha" #f)
                                  (pkeyword 'unstable "x86" "linux")
                                  (pkeyword 'unstable "amd64" #f))))
 ]
}
