;; This file is part of racket-pmsf - PMS formatting and parsing functions.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-pmsf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-pmsf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-pmsf.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require (for-label racket
                     typed/pmsf))


@title[#:tag "pmsf-typed"]{Typed interface}


@defmodule[typed/pmsf]

@racket[typed/pmsf] library re-provides all PMSF typed interface bindings.

Libraries under @racketid[typed/pmsf/] provide specified typed interfaces.


@section[#:tag "pmsf-typed-condition"]{Typed condition}

@defmodule[typed/pmsf/condition]

Typed interface to @racket[pmsf/condition].


@section[#:tag "pmsf-typed-name"]{Typed name}

@defmodule[typed/pmsf/name]

Typed interface to @racket[pmsf/name].


@section[#:tag "pmsf-typed-depend"]{Typed DEPEND}

@defmodule[typed/pmsf/depend]

Typed interface to @racket[pmsf/depend].


@section[#:tag "pmsf-typed-iuse"]{Typed IUSE}

@defmodule[typed/pmsf/iuse]

Typed interface to @racket[pmsf/iuse].


@section[#:tag "pmsf-typed-keywords"]{Typed KEYWORDS}

@defmodule[typed/pmsf/keywords]

Typed interface to @racket[pmsf/keywords].


@section[#:tag "pmsf-typed-license"]{Typed LICENSE}

@defmodule[typed/pmsf/license]

Typed interface to @racket[pmsf/license].


@section[#:tag "pmsf-typed-required-use"]{Typed REQUIRED_USE}

@defmodule[typed/pmsf/required-use]

Typed interface to @racket[pmsf/required-use].


@section[#:tag "pmsf-typed-restrict"]{Typed RESTRICT}

@defmodule[typed/pmsf/restrict]

Typed interface to @racket[pmsf/restrict].


@section[#:tag "pmsf-typed-slot"]{Typed SLOT}

@defmodule[typed/pmsf/slot]

Typed interface to @racket[pmsf/slot].


@section[#:tag "pmsf-typed-src-uri"]{Typed SRC_URI}

@defmodule[typed/pmsf/src-uri]

Typed interface to @racket[pmsf/src-uri].


@section[#:tag "pmsf-typed-manifest"]{Typed Manifest}

@defmodule[typed/pmsf/manifest]

Typed interface to @racket[pmsf/manifest].
