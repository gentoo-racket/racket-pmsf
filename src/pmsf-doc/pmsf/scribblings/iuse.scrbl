;; This file is part of racket-pmsf - PMS formatting and parsing functions.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-pmsf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-pmsf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-pmsf.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require (for-label racket
                     pmsf/iuse)
          scribble/example)


@(define example-evaluator
   (make-base-eval '(require pmsf/iuse)))


@title[#:tag "pmsf-iuse"]{IUSE}

@defmodule[pmsf/iuse]


@section[#:tag "pmsf-iuse-struct"]{IUSE structs}

@defstruct[
 piuse
 ([default piuse-default?]
  [flag    string?])
 #:transparent
 ]{

 @examples[
 #:eval example-evaluator
 (piuse 'enabled "wayland")
 ]
}

@defstruct[
 piuses
 ([iuses (listof iuse?)])
 #:transparent
 ]{

 @examples[
 #:eval example-evaluator
 (piuses (list (piuse 'enabled "wayland")))
 ]
}


@section[#:tag "pmsf-iuse-helpers"]{IUSE helper functions}

@defproc[
 (piuse-default? [v any])
 boolean?
 ]{

 @examples[
 #:eval example-evaluator
 (piuse-default? 'disabled)
 (piuse-default? 'enabled)
 (piuse-default? #false)
 (piuse-default? "asd")
 ]
}


@section[#:tag "pmsf-iuse-convert"]{IUSE conversion}

@defproc[
 (piuse-default->string [a-piuse-default piuse-default?])
 string?
 ]{

 @examples[
 #:eval example-evaluator
 (piuse-default->string 'enabled)
 ]
}

@defproc[
 (piuse->string [a-piuse piuse?])
 string?
 ]{

 @examples[
 #:eval example-evaluator
 (piuse->string (piuse 'enabled "wayland"))
 ]
}

@defproc[
 (piuses->string [a-piuses piuses?])
 string?
 ]{

 @examples[
 #:eval example-evaluator
 (piuses->string (piuses (list (piuse 'enabled "wayland"))))
 ]
}

@defproc[
 (string->piuse [a-string string?])
 piuse?
 ]{

 @examples[
 #:eval example-evaluator
 (string->piuse "+wayland")
 ]
}

@defproc[
 (string->piuses [a-string string?])
 piuses?
 ]{

 @examples[
 #:eval example-evaluator
 (string->piuses "+wayland")
 ]
}
