;; This file is part of racket-pmsf - PMS formatting and parsing functions.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-pmsf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-pmsf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-pmsf.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require (for-label racket
                     pmsf/slot)
          scribble/example
          pmsf/slot)


@(define example-evaluator
   (make-base-eval '(require pmsf/slot)))


@title[#:tag "pmsf-slot"]{SLOT}

@defmodule[pmsf/slot]


@section[#:tag "pmsf-slot-special"]{SLOT special elements}

@defproc[
 (slot-name-string?
  [v any])
 boolean?
 ]{

 @examples[
 #:eval example-evaluator
 (slot-name-string? "1.2.3")
 ]
}


@section[#:tag "pmsf-slot-struct"]{SLOT struct}

@defmodule[pmsf/slot/struct]

@defstruct[
 pslot
 ([supername slot-name-string?]
  [subname   (or/c #false slot-name-string?)])
 #:transparent
 ]{

 @examples[
 #:eval example-evaluator
 (pslot "1.2.3" "4")
 ]
}


@section[#:tag "pmsf-slot-convert"]{SLOT conversion}

@defmodule[pmsf/slot/convert]

@defproc[
 (pslot->string
  [input-pslot pslot?])
 string?
 ]{

 @examples[
 #:eval example-evaluator
 (pslot->string (pslot "1.2.3" #false))
 (pslot->string (pslot "1.2.3" "4"))
 ]
}

@defproc[
 (port->pslot [input-port input-port?])
 pslot?
 ]{

}

@defproc[
 (string->pslot
  [input-string string?])
 pslot?
 ]{

 @examples[
 #:eval example-evaluator
 (string->pslot "1.2.3")
 (string->pslot "1.2.3/4")
 ]
}
