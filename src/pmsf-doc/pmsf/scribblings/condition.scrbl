;; This file is part of racket-pmsf - PMS formatting and parsing functions.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-pmsf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-pmsf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-pmsf.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require (for-label racket
                     pmsf/condition)
          scribble/example
          pmsf/condition)


@(define example-evaluator
   (make-base-eval '(require pmsf/condition)))


@title[#:tag "pmsf-condition"]{Condition}

@defmodule[pmsf/condition]


@section[#:tag "pmsf-condition-special"]{Condition special elements}

@defmodule[pmsf/condition/condition]

@defproc[
 (special-condition?
  [v any])
 boolean?
 ]{

 @examples[
 #:eval example-evaluator
 (special-condition? 'at-most-one)
 (special-condition? 'exclusive-or)
 (special-condition? 'inclusive-or)
 ]
}

@defproc[
 (flag-condition?
  [v any])
 boolean?
 ]{

 @examples[
 #:eval example-evaluator
 (flag-condition? "test")
 (flag-condition? '(not . "test"))
 ]
}

@defproc[
 (condition?
  [v any])
 boolean?
 ]{

 @examples[
 #:eval example-evaluator
 (condition? 'at-most-one)
 (condition? "test")
 ]
}

@defproc[
 (condition-string?
  [v any])
 boolean?
 ]{

 @examples[
 #:eval example-evaluator
 (condition-string? "test?")
 (condition-string? "!test?")
 (condition-string? "??")
 (condition-string? "^^")
 (condition-string? "||")
 ]
}


@section[#:tag "pmsf-condition-struct"]{Condition struct}

@defmodule[pmsf/condition/struct]

@defstruct[
 pcondition
 ([condition condition?]
  [contents  (listof any)])
 #:transparent
 ]{

 @examples[
 #:eval example-evaluator
 (pcondition '(not . "test") '("test"))
 ]
}


@section[#:tag "pmsf-condition-convert"]{Condition conversion}

@defmodule[pmsf/condition/convert]

@defproc[
 (condition->string
  [input-condition condition?])
 condition-string?
 ]{

 @examples[
 #:eval example-evaluator
 (condition->string "test")
 (condition->string '(not . "test"))
 (condition->string 'at-most-one)
 (condition->string 'exclusive-or)
 (condition->string 'inclusive-or)
 ]
}

@defproc[
 (string->condition
  [input-string condition-string?])
 condition?
 ]{

 @examples[
 #:eval example-evaluator
 (string->condition "test?")
 (string->condition "!test?")
 (string->condition "^^")
 (string->condition "??")
 (string->condition "||")
 ]
}


@section[#:tag "pmsf-condition-query"]{Condition querying}

@defmodule[pmsf/condition/query]

@defproc[
 (pcondition->contents
  [input-pcondition pcondition?])
 (listof any)
 ]{
 Extract contents of given @racket[pcondition],
 all @racketid[condition]s are thrown away.

 @examples[
 #:eval example-evaluator
 (pcondition->contents
  (pcondition "gui"
              (list "client"
                    (pcondition "qt5"
                                (list '(not . "aqua")
                                      "X")))))
 ]
}
