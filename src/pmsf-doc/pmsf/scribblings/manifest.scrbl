;; This file is part of racket-pmsf - PMS formatting and parsing functions.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-pmsf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-pmsf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-pmsf.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require (for-label racket
                     pmsf/manifest)
          scribble/example
          pmsf/manifest)


@(define glep74-url
   "https://www.gentoo.org/glep/glep-0074.html")

@(define example-evaluator
   (make-base-eval '(require pmsf/manifest)))


@title[#:tag "pmsf-manifest"]{Manifest}

@defmodule[pmsf/manifest]

PMSF Manifest structures and support utilities implementation is primarily
based on the @link[glep74-url]{GLEP 74} document.


@section[#:tag "pmsf-manifest-struct"]{Manifest structs}

@defmodule[pmsf/manifest/struct]

@defstruct[
 checksum
 ([algorithm string?]
  [sum       string?])
 #:transparent
 ]{

 @examples[
 #:eval example-evaluator
 (checksum "BLAKE2B" "7ff24aa2e16a")
 ]
}

@defstruct[
 pathcheck
 ([path      path-string?]
  [size      exact-nonnegative-integer?]
  [checksums (listof checksum?)])
 #:transparent
 ]{

 @examples[
 #:eval example-evaluator
 (pathcheck "file" 0 '())
 ]
}

@defstruct[
 (submanifest pathcheck)
 ()
 #:transparent
 ]{

 @examples[
 #:eval example-evaluator
 (submanifest "app-accessibility/Manifest"
              14821
              (list (checksum "SHA256" "1b5f")
                    (checksum "SHA512" "f7eb")))
 ]
}

@defstruct[
 (data pathcheck)
 ()
 #:transparent
 ]{

 @examples[
 #:eval example-evaluator
 (data "SphinxTrain-1.0.8.ebuild"
       912
       (list (checksum "SHA256" "f681")
             (checksum "SHA512" "0749")))
 ]
}

@defstruct[
 (dist pathcheck)
 ()
 #:transparent
 ]{

 @examples[
 #:eval example-evaluator
 (dist "sphinxtrain-1.0.8.tar.gz"
       8925803
       (list (checksum "SHA256" "548e")
             (checksum "SHA512" "465d")))
 ]
}

@defstruct[
 (aux pathcheck)
 ()
 #:transparent
 ]{
 For backwards compatibility.

 See @link[(string-append glep74-url "#deprecated-manifest-tags")
           ]{GLEP 74 "Deprecated Manifest tags"}.

 @examples[
 #:eval example-evaluator
 (data "files/gcc34.patch"
       333
       (list (checksum "SHA256" "c107") (checksum "SHA512" "9919")))
 ]
}

@defstruct[
 manifest
 ([timestamp    date?]
  [ignore       (listof path-string?)]
  [submanifests (listof submanifest?)]
  [data         (listof data?)]
  [dist         (listof dist?)]
  [aux          (listof aux?)])
 #:transparent
 ]{

 @examples[
 #:eval example-evaluator
 (manifest (date* 12 11 10 30 10 2017 1 302 #f 3600 0 "")
           '("distfiles" "local" "lost+found" "packages")
           (list (submanifest "app-accessibility/Manifest"
                              14821
                              (list (checksum "SHA256" "1b5f")
                                    (checksum "SHA512" "f7eb")))
                 (submanifest "eclass/Manifest.gz"
                              50812
                              (list (checksum "SHA256" "8c55")
                                    (checksum "SHA512" "2915"))))
           '()
           '()
           '())
 (manifest #false
           '()
           '()
           (list (data "SphinxTrain-1.0.8.ebuild"
                       912
                       (list (checksum "SHA256" "f681")
                             (checksum "SHA512" "0749")))
                 (data "metadata.xml"
                       664
                       (list (checksum "SHA256" "97c6")
                             (checksum "SHA512" "1175")))
                 (data "files/gcc34.patch"
                       333
                       (list (checksum "SHA256" "c107")
                             (checksum "SHA512" "9919"))))
           (list (dist "sphinxtrain-1.0.8.tar.gz"
                       8925803
                       (list (checksum "SHA256" "548e")
                             (checksum "SHA512" "465d"))))
           '())
 ]
}


@section[#:tag "pmsf-manifest-convert"]{Manifest conversion}

@defmodule[pmsf/manifest/convert]

@defproc[
 (manifest->string [input-manifest manifest?])
 string?
 ]{

 @examples[
 #:eval example-evaluator
 (manifest->string (manifest #false
                             '()
                             '()
                             (list (data "metadata.xml"
                                         664
                                         (list (checksum "SHA256" "97c6")
                                               (checksum "SHA512" "1175"))))
                             '()
                             '()))
 ]
}

@defproc[
 (port->manifest [input-port input-port?])
 manifest?
 ]{

}

@defproc[
 (string->manifest [input-string string?])
 manifest?
 ]{

 @examples[
 #:eval example-evaluator
 (string->manifest "DATA metadata.xml 664 SHA256 97c6 SHA512 1175")
 ]
}
