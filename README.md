# Package Manager Specification Formats

<p align="center">
    <a href="https://archive.softwareheritage.org/browse/origin/?origin_url=https://gitlab.com/gentoo-racket/racket-pmsf">
        <img src="https://archive.softwareheritage.org/badge/origin/https://gitlab.com/gentoo-racket/racket-pmsf/">
    </a>
    <a href="https://gitlab.com/gentoo-racket/racket-pmsf/pipelines">
        <img src="https://gitlab.com/gentoo-racket/racket-pmsf/badges/master/pipeline.svg">
    </a>
</p>

Package Manager Specification formatting and parsing functions.


## About

Racket library to providing PMS formatting and parsing functions.

The Package Manager Specification document:
[dev.gentoo.org/~ulm/pms](https://dev.gentoo.org/~ulm/pms/head/pms.html)

### Components

#### PMS

- `pmsf-depend` for `DEPEND` syntax

  ```sh
  DEPEND="ssl? ( dev-libs/openssl:= )"
  ```

- `pmsf-iuse` for `IUSE` syntax

  ```sh
  IUSE="+jit threads -minimal"
  ```

- `pmsf-keywords` for `KEYWORDS` syntax

  ```sh
  KEYWORDS="~amd64-linux"
  ```

- `pmsf-license` for `LICENSE` syntax

  ```sh
  LICENSE="|| ( Apache-2.0 MIT )"
  ```

- `pmsf-name` for package name syntax

  ```sh
  dev-libs/openssl-1.1.1q
  ```

- `pmsf-required-use` for `REQUIRED_USE` synatx

  ```sh
  REQUIRED_USE="test? ( clang )"
  ```

- `pmsf-restrict` for `RESTRICT` synatx

  ```sh
  RESTRICT="!test? ( test )"
  ```

- `pmsf-slot` for `SLOT` synatx

  ```sh
  SLOT="0/1.1"
  ```

- `pmsf-src-uri` for `SRC_URI` synatx

  ```sh
  SRC_URI="mirror://openssl/source/openssl-1.1.1q.tar.gz
      verify-sig? ( mirror://openssl/source/openssl-1.1.1q.tar.gz.asc )"
  ```

#### GLEP

- `pmsf-manifest` for `Manifest` files syntax
  ([GLEP 74](https://www.gentoo.org/glep/glep-0074.html))

  ```conf
  TIMESTAMP 2017-10-30T10:11:12Z
  IGNORE distfiles
  MANIFEST eclass/Manifest.gz 50812 SHA256 8c55 SHA512 2915
  DATA SphinxTrain-0.9.1-r1.ebuild 932 SHA256 3d3b SHA512 be4d
  DIST SphinxTrain-0.9.1-beta.tar.gz 469617 SHA256 c1a4 SHA512 1b33
  ```


## Installation

### Make

Use GNU Make to install Racket-PMSF from its project directory.

```sh
make install
```

### Raco

Use raco to install Racket-PMSF from the official Racket Package Catalog.

```sh
raco pkg install --auto --skip-installed --user pmsf
```

### Req

Use Req to install Racket-PMSF from its project directory.

``` sh
raco req --everything --verbose
```


## Documentation

Documentation can either be built locally with `make public`
or browsed online on either
[GitLab pages](https://gentoo-racket.gitlab.io/racket-pmsf/)
or [Racket-Lang Docs](https://docs.racket-lang.org/pmsf/).


## License

Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>

Licensed under the GNU GPL v2 License

SPDX-License-Identifier: GPL-2.0-or-later
