MAKE            ?= make
RACKET          := racket
RACO            := $(RACKET) -l raco --
SCRIBBLE        := $(RACO) scribble

PWD             := $(shell pwd)
DOCS            := $(PWD)/docs
PUBLIC          := $(PWD)/public
SCRIPTS         := $(PWD)/scripts
SRC             := $(PWD)/src
TESTS           := $(PWD)/tests


.PHONY: all
all: compile

src-%:
	$(MAKE) -C $(SRC) $(*)

.PHONY: clean
clean: src-clean

.PHONY: compile
compile: src-compile

.PHONY: install
install: src-install

.PHONY: setup
setup: install
setup:
	$(MAKE) -B src-setup

.PHONY: shellcheck
shellcheck:
	find $(PWD) -type f -name "*.sh" -exec shellcheck {} +

test-prepare: src-install-pkg-pmsf-test

.PHONY: test-unit
test-unit: test-prepare
test-unit:
	$(MAKE) -B -C $(TESTS) unit

.PHONY: test-integration
test-integration: test-prepare
test-integration:
	$(MAKE) -B -C $(TESTS) integration

.PHONY: test
test: test-unit
test: test-integration

.PHONY: test-full
test-full: install
test-full:
	$(MAKE) -B compile
	$(MAKE) -B src-test

.PHONY: remove
remove: src-remove

$(PUBLIC):
	$(SCRIBBLE) ++main-xref-in --dest $(PWD) --dest-name public \
		--htmls --quiet $(DOCS)/scribblings/main.scrbl

.PHONY: public
public:
	$(MAKE) -B $(PUBLIC)
